Strawberry-flavored JWM
==============================================================================

[한국어](./README.korean.md)

An unofficial modification of JWM s1685. ([Original](https://github.com/joewing/jwm/releases/tag/s1685))

As of 2022/06/07:

 * Simpler build script
 * Removed tray and desktop menu by default (You can revive it in settings.xml)
 * Pink background and black text
  * Gray background for inactive
 * No border by default
 * Default font is 'Roboto 12'
 * Default settings file is /etc/jwm-mod/settings.xml
 * Click to focus on window by default
 * Alt+Tab works like Xfwm4 and/or Openbox
 * Title bar thickness is ((Lesser one between width and height of screen) / 64) * 3 if 'WindowStyle/Height' is not set
